## Installation

1. `git clone https://trojanj@bitbucket.org/trojanj/etherscan.git`
2. `cd etherscan`
3. Rename `.env.example` to `.env`. Add `REACT_APP_ADDRESS` and `REACT_APP_API_KEY` variables. 
You can get free API key here: https://amberdata.io/docs/onboarding#step-2-get-an-api-key
3. `npm i`
4. `npm start`