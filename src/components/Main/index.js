import React, { useEffect, useState } from 'react';
import classes from './styles.module.sass'
import axios from 'axios';
import { env } from '../../env';
import { web3 } from '../../configs/web3';

export const Main = () => {
  const [balance, setBalance] = useState(0);
  const [currency, setCurrency] = useState(0);
  const [tokens, setTokens] = useState([]);
  const { apiKey, address } = env;

  useEffect(() => {
    try {
      (async () => {
        const response = await axios({
          method: 'get',
          url: `https://web3api.io/api/v2/addresses/${address}/account-balances/latest`,
          headers: { 'x-api-key': apiKey },
          params: { includePrice: true }
        });
        setBalance(web3.utils.fromWei(response.data.payload.value));
        setCurrency(Number(response.data.payload.price.value.quote));
      })();
    } catch (e) {
      console.log(e.message);
    }
  }, [apiKey, address]);

  useEffect(() => {
    try {
      (async () => {
        const response = await axios({
          method: 'get',
          url: `https://web3api.io/api/v1/addresses/${address}/token-balances/latest`,
          headers: { 'x-api-key': apiKey }
        });
        setTokens(response.data.payload.records);
      })();
    } catch (e) {
      console.log(e.message);
    }
  }, [apiKey, address]);

  const renderTokens = () => (
    tokens.map(token => (
      <div className="row py-2" key={token.address}>
        <div className="col-4">{token.symbol || token.name || 'Unnamed token'}</div>
        <div className="col-8">{token.amount}</div>
      </div>
    ))
  );

  return (
    <div className={classes.Main}>
      <div className="container">
        <div className={classes.addressWrp}>
          <span className={classes.addressTitle}>Address: </span>
          <span className={classes.address}>{address}</span>
        </div>

        <div className={`${classes.table} container mb-3`}>
          <div className="row py-2">
            <div className="col-12 font-weight-bolder">Overview</div>
          </div>

          <div className="row py-2">
            <div className="col-4">Balance:</div>
            <div className="col-8">
              {`${balance} Ether`}
            </div>
          </div>

          <div className="row py-2">
            <div className="col-4">Ether Value:</div>
            <div className="col-8">
              {`$${(balance * currency).toFixed(2)}`}
              <span className={classes.additional}>{` (@ $${currency.toFixed(2)}/ETH)`}</span>
            </div>
          </div>
        </div>

        <div className={`${classes.table} container mb-3`}>
          <div className="row py-2">
            <div className="col-12 font-weight-bolder">Tokens balance</div>
          </div>

          {renderTokens()}
        </div>
      </div>
    </div>
  );
};
