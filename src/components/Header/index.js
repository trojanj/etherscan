import React from 'react';
import classes from './styles.module.sass';

export const Header = () => {
  return (
    <div className={classes.Header}>
      <div className="container">Etherscan 0.0.1</div>
    </div>
  );
};