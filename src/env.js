import { getOsEnv } from './helpers/pathHelper';

export const env = {
  apiKey: getOsEnv('REACT_APP_API_KEY'),
  address: getOsEnv('REACT_APP_ADDRESS')
};
